package com.edu.uclm.procesos.server.dao;

import java.util.List;

import com.edu.uclm.procesos.server.model.Person;


public interface PersonDao {

	void savePerson(Person person);

	void deletePerson(Person person);

	void updatePerson(Person person);

	List<Person> getAllPerson();

	Person getPerson(String id_person);

	
}
