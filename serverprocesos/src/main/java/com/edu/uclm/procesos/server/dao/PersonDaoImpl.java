package com.edu.uclm.procesos.server.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.edu.uclm.procesos.server.model.Person;

@Repository
public class PersonDaoImpl implements PersonDao {

	@Autowired
	MongoOperations mongoTemplate;

	@Override
	public void savePerson(Person person) {
		mongoTemplate.save(person);

	}

	@Override
	public void deletePerson(Person person) {
		mongoTemplate.remove(person);

	}

	@Override
	public void updatePerson(Person person) {
		mongoTemplate.save(person);
	}

	@Override
	public Person getPerson(String id_person) {
		Query query = new Query(Criteria.where("_id").is(id_person));

		List<Person> report = mongoTemplate.find(query, Person.class);
		if (report.size() != 0) {
			return report.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<Person> getAllPerson() {
		List<Person> list = mongoTemplate.findAll(Person.class);
		return list;
	}
}
