package com.edu.uclm.procesos.server.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.edu.uclm.procesos.server.model.Person;
import com.edu.uclm.procesos.server.service.PersonService;

@RestController
@RequestMapping(value = "/person")
public class PersonController {

	@Autowired
	PersonService personService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public void savePerson(@RequestBody Person person) {

		this.personService.savePerson(person);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deletePerson(@RequestBody Person person) {

		this.personService.deletePerson(person);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void updatePerson(@RequestBody Person person) {

		this.personService.updatePerson(person);
	}

	@RequestMapping(value = "/get")
	public List<Person> getAllPerson() {
		List<Person> listperson = this.personService.getAllPerson();
		return listperson;
	}

	@RequestMapping(value = "get/{id}")
	public Person getReportById(@PathVariable(value = "id") String id) {
		Person person = this.personService.getPerson(id);
		return person;
	}
}
