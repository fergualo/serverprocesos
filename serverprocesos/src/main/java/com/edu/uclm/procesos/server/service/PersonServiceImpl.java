package com.edu.uclm.procesos.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.uclm.procesos.server.dao.PersonDao;
import com.edu.uclm.procesos.server.model.Person;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonDao personDao;

	@Override
	public void savePerson(Person person) {
		personDao.savePerson(person);
	}

	@Override
	public void deletePerson(Person person) {
		personDao.deletePerson(person);
	}

	@Override
	public void updatePerson(Person person) {
		personDao.updatePerson(person);
	}

	@Override
	public List<Person> getAllPerson() {
		return personDao.getAllPerson();
	}

	@Override
	public Person getPerson(String id_person) {
		return personDao.getPerson(id_person);
	}

}
