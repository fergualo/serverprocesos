package com.edu.uclm.procesos.server.service;

import java.util.List;

import com.edu.uclm.procesos.server.model.Person;

public interface PersonService {
	void savePerson(Person person);

	void deletePerson(Person person);

	void updatePerson(Person person);

	List<Person> getAllPerson();

	Person getPerson(String id_person);

}
